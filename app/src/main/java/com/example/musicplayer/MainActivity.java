package com.example.musicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    TextView nameOfSong;
    SeekBar time;
    TextView timeToStart;
    TextView timeToFinish;
    Button btnPlay;
    MediaPlayer mp;
    int totalTime;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlay = findViewById(R.id.btnPlay);
        timeToStart = findViewById(R.id.timeToStart);
        timeToFinish = findViewById(R.id.timeToFinish);

        nameOfSong = findViewById(R.id.nameOfSong);
        nameOfSong.setText(R.raw.kino_peremen);
        File name = new File((String) nameOfSong.getText());
        nameOfSong.setText(name.getName());

        mp = MediaPlayer.create(this, R.raw.kino_peremen);
        mp.setLooping(false);
        mp.seekTo(0);
        mp.setVolume(0,1);
        totalTime = mp.getDuration();

        time = findViewById(R.id.time);
        time.setMax(totalTime);
        time.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(fromUser){
                            mp.seekTo(progress);
                            time.setProgress(progress);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );

        new Thread(new Runnable() {
            @Override
            public void run() {
                while ((mp != null)){
                    try{
                        Message msg = new Message();
                        msg.what = mp.getCurrentPosition();
                        handler.sendMessage(msg);
                        Thread.sleep(1000);
                    } catch (Exception ignored){ }
                }
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler(){
        @SuppressLint("SetTextI18n")
        @Override
        public void handleMessage(@NonNull Message msg) {
            int currentPosition = msg.what;
            time.setProgress(currentPosition);
            String start = createTimeLabel(currentPosition);
            timeToStart.setText(start);

            String finish = createTimeLabel(totalTime - currentPosition);
            timeToFinish.setText("- " + finish);
        }
    };

    public String createTimeLabel(int time){
        String start;
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        start = min + ":";
        if(sec < 10){
            start += "0";
        }
        start += sec;
        return start;
    }

    public void onClickBtnPlay(View v){
        if(!mp.isPlaying()){
            mp.start();
            btnPlay.setBackgroundResource(R.drawable.pause);
        } else {
            mp.pause();
            btnPlay.setBackgroundResource(R.drawable.play);
        }
    }
}
